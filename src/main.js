import { createApp } from 'vue'
import App from './App.vue'
import store from './store';
import componentsUI from '@/components/UI';
import functionGlobal from "./globalFunction/handlingString";

const app = createApp(App);

functionGlobal.forEach( functionMe => {
  app.config.globalProperties['$' + functionMe.name] = functionMe;
})

componentsUI.forEach( component => {
  app.component(component.name, component);
})

app.use(store);
app.mount("#app");


