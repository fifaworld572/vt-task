export default {
    namespaced: true,
    state: () =>({
        dataUser: [
            {
                id: 1675948958954,
                name: 'Sasha',
            },
            {
                id: 1675948958955,
                name: 'Denis',
            },
            {
                id: 1675948958956,
                name: 'Kostya',
            },
            {
                id: 1675948958957,
                name: 'Vlad',
            },
            {
                id: 1675948958958,
                name: 'Oleg',
            },
            {
                id: 1675948958959,
                name: 'Pavel',
            }
        ],
    }),

    mutations: {
        deleteUserStore(state, value) {
            console.log('test', value)
            const newDataUsers = state.dataUser.filter(el => el.id !== value);

            return state.dataUser = newDataUsers
        },
        addNewUser(state, name) {
            const newUser = {
                id: new Date().getTime(),
                name,
            }

            state.dataUser = [...state.dataUser, newUser]
        },

    },
}
